﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CanEditMultipleObjects]
[CustomEditor(typeof(ScreenshotTaker))]
public class ScreenshotTakerEditor : Editor
{

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        var screenshotTaker = (ScreenshotTaker)target;

        EditorGUILayout.LabelField("Resolution", EditorStyles.boldLabel);
        screenshotTaker.resWidth = EditorGUILayout.IntField("Width", screenshotTaker.resWidth);
        screenshotTaker.resHeight = EditorGUILayout.IntField("Height", screenshotTaker.resHeight);

        EditorGUILayout.Space();

        screenshotTaker.scale = EditorGUILayout.IntSlider("Scale", screenshotTaker.scale, 1, 15);

        EditorGUILayout.HelpBox("The default mode of screenshot is crop - so choose a proper width and height. The scale is a factor " +
                                "to multiply or enlarge the renders without loosing quality.", MessageType.None);


        EditorGUILayout.Space();


        GUILayout.Label("Save Path", EditorStyles.boldLabel);

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.TextField(screenshotTaker.path, GUILayout.ExpandWidth(false));
        if (GUILayout.Button("Browse", GUILayout.ExpandWidth(false)))
            screenshotTaker.path = EditorUtility.SaveFolderPanel("Path to Save Images", screenshotTaker.path, Application.dataPath);

        EditorGUILayout.EndHorizontal();

        EditorGUILayout.HelpBox("Choose the folder in which to save the screenshots ", MessageType.None);
        EditorGUILayout.Space();



        //isTransparent = EditorGUILayout.Toggle(isTransparent,"Transparent Background");



        GUILayout.Label("Select Camera", EditorStyles.boldLabel);


        screenshotTaker.myCamera = EditorGUILayout.ObjectField(screenshotTaker.myCamera, typeof(Camera), true, null) as Camera;


        if (screenshotTaker.myCamera == null)
        {
            screenshotTaker.myCamera = Camera.main;
        }


        EditorGUILayout.HelpBox("Choose the camera of which to capture the render.", MessageType.None);

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Screenshot will be taken at " + screenshotTaker.resWidth * screenshotTaker.scale + " x " + screenshotTaker.resHeight * screenshotTaker.scale + " px", EditorStyles.boldLabel);

        if (GUILayout.Button("Take Screenshot", GUILayout.MinHeight(60)))
        {
            if (screenshotTaker.path == "")
            {
                screenshotTaker.path = EditorUtility.SaveFolderPanel("Path to Save Images", screenshotTaker.path, Application.dataPath);
                Debug.Log("Path Set");
                TakeHiResShot();
            }
            else
            {
                TakeHiResShot();
            }
        }


        if (screenshotTaker.takeHiResShot)
        {
            int resWidthN = screenshotTaker.resWidth * screenshotTaker.scale;
            int resHeightN = screenshotTaker.resHeight * screenshotTaker.scale;
            RenderTexture rt = new RenderTexture(resWidthN, resHeightN, 24);
            screenshotTaker.myCamera.targetTexture = rt;

            TextureFormat tFormat;
            if (screenshotTaker.isTransparent)
                tFormat = TextureFormat.ARGB32;
            else
                tFormat = TextureFormat.RGB24;


            Texture2D screenShot = new Texture2D(resWidthN, resHeightN, tFormat, false);
            screenshotTaker.myCamera.Render();
            RenderTexture.active = rt;
            screenShot.ReadPixels(new Rect(0, 0, resWidthN, resHeightN), 0, 0);
            screenshotTaker.myCamera.targetTexture = null;

            screenShot = TextureScaler.scaled(screenShot, screenshotTaker.resWidth, screenshotTaker.resHeight, FilterMode.Trilinear);

            RenderTexture.active = null;
            byte[] bytes = screenShot.EncodeToPNG();
            string filename = ScreenShotName(resWidthN, resHeightN);

            System.IO.File.WriteAllBytes(filename, bytes);
            Debug.Log(string.Format("Took screenshot to: {0}", filename));
            Application.OpenURL(filename);
            screenshotTaker.takeHiResShot = false;
        }

        serializedObject.ApplyModifiedProperties();
    }


    public string ScreenShotName(int width, int height)
    {

        var screenshotTaker = (ScreenshotTaker)target;
        string strPath = "";

        strPath = string.Format("{0}/screen_{1}x{2}_{3}.png",
            screenshotTaker.path,
            width, height,
            System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
        screenshotTaker.lastScreenshot = strPath;

        return strPath;
    }



    public void TakeHiResShot()
    {
        var screenshotTaker = (ScreenshotTaker)target;
        Debug.Log("Taking Screenshot");
        screenshotTaker.takeHiResShot = true;
    }

}