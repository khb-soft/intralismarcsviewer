﻿//C# Example
using UnityEngine;


public class ScreenshotTaker : MonoBehaviour
{
    [SerializeField]
    public int resWidth = 512;
    [SerializeField]
    public int resHeight = 512;
    [SerializeField]
    public Camera myCamera;
    [SerializeField]
    public int scale = 4;
    [SerializeField]
    public string path = "";
    [SerializeField]
    public bool showPreview = true;

    public RenderTexture renderTexture;

    [SerializeField]
    public bool isTransparent = false;

    public float lastTime;

    public bool takeHiResShot = false;
    public string lastScreenshot = "";
}