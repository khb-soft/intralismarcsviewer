# Intralism Arcs Viewer
by Oxy949
### How to create arcs for Intralism?
**Preparations:**

1.1. Download and install Unity 2018.4.6 from UnityHub (https://unity3d.com/get-unity/download);

1.2. Download the ArcsViewer project (this) from bitbucket ("downloads" button on the left panel on project's page);

1.3. Open this project in Unity;

1.4. Open main scene (Assets/ArcsViewerScene.unity);



**Creation:**

2.1. Copy "\_ArcsTemplate/player.yournameher" folder to "Assets/Arcs" folder outside Unity (use Windows Explorer and others);

2.2. Rename copyed folder to your "player.[YOUR ID]" where [YOUR ID] is any text you want to idintify your arcs (like "player.neonarks" or "player.dragon");

2.3. Switch back to Unity and go to "Assets/Arcs/player.[YOUR ID]/" folder in Project Window. This is your project files. "Segment-[\*\*\*\*]" - prefabs of arcs that Intralism spawns in-game. Edit them as you want (change mesh and colors, add new scripts and objects);

2.4. Use "Paste Arcs HERE" object in scene Hierarchy Window to drop arcs prefab inside it to preview arcs like in game.



**Finishing:**

3.1. Make shure that all of your project files located inside "Assets/Arcs/player.[YOUR ID]/" folder and has no outside dependence (like material in other folder);

3.2. Make shure that "Assets/Arcs/player.[YOUR ID]/" folder contains all four "Segment-[\*\*\*\*]" prefabs that hasn't any dependence;

3.3. Drag "Segment-[Left]" and "Segment-[Right]" prefabs from "Assets/Arcs/player.[YOUR ID]/" folder into "Paste Arcs HERE" object in scene Hierarchy Window (like in paragraf 2.4). Than select "\_Screenshot Maker" object in scene Hierarchy Window and press "Browse" button to select screenshot's location. I recomend you to select Desktop folder. Than press big "Take Screenshot" button to generate preview icon of you arcs;



**Uploading:**

4.1. Open Intralism game and go "Workshop -> Items Uploader" from main menu;

4.2. Create new project with "[YOUR ID]" name;

4.3. Press "Open content folder" button; Copy there "Assets/Arcs/player.[YOUR ID]/" folder;

4.4. Select preview file icon, generated in paragraf 3.3;

4.5. Enter title, description and make shure that project item type is "Arcs";

4.6. When it's done finally press Publish To Workshop button;

4.7. **IMPORTANT!** After seccessfull publishing open item's page to fill payment options and configure revenue
